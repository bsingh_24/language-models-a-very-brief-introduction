# Language Models - a Very Brief Introduction

This is the material for the introductory course to Language Models.

- the PowerPoint slides are in the pptx folder
- the reading_material folder contains the research papers covered in the course (plus some others)
- the notebooks folder contains the notebooks that accompany the course

# To run the notebooks (Windows)

**NOTE:** It is advisable to have a GPU-equipped machine. Please refer to these links:
	- [https://docs.nvidia.com/cuda/cuda-installation-guide-microsoft-windows/contents.html](CUDA documentation)
	- [https://developer.nvidia.com/cuda-downloads](CUDA downloads)

1. install [https://www.anaconda.com/download/](anaconda)
2. launch anaconda navigator
3. [https://docs.anaconda.com/free/navigator/getting-started/](create a virtual environment)
	- make sure you use **python 3.9.***
4. install **CMD.exe prompt** and **Notebook**
5. launch the prompt 
6. navigate to the folder containing this README.md file
7. update pip: 
	- ```python.exe -m pip install --upgrade pip```
8. install the requirements:
	- ```python.exe -m pip install -r requirements.txt```
8. uninstall some packages:
	- ```pip uninstall torch torchvision torchaudio transformers```
9. install pytorch with CUDA extensions [https://pytorch.org/get-started/locally/](here)
10. install transformers:
	- ```pip install transformers```
11. install RISE (for the slideshow)
	- ```conda install -c conda-forge rise```
12. enable ipywidgets (for the slideshow):
	- ```jupyter nbextension enable --py widgetsnbextension```

You should now be ready to launch  Notebook, open the notebooks, run the code and in presentation mode as well!	
	